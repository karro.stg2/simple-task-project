import { Validators } from '@angular/forms';

export const GOOGLE_MAPS_API_KEY = 'AIzaSyBHRPOa9jHbPo9PjAbMXsT4y7-eF6reoEs';
export const IMGUR_API_KEY = 'a1d49543e9f49b6';

export const TEXT_INPUT_VALIDATORS = Validators.compose([Validators.required, Validators.maxLength(50)]);
export const TEXT_AREA_VALIDATORS = Validators.compose([Validators.required, Validators.maxLength(250)]);
export const DISCOUNT_VALIDATORS = Validators.compose([Validators.min(7), Validators.max(100)]);


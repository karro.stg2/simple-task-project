import { Observable } from 'rxjs/Observable';

export interface ImageUploaderInterface {

    upload(data: Blob): Promise<any>;
}

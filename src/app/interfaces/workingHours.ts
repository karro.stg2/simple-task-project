export enum WorkingHoursState {
    'Custom' = 'Custom',
    'Closed' = 'Closed',
    'Full' = 'Full'
}
export interface WorkingHours {
    state: WorkingHoursState;
    timeFrom: string;
    timeTo: string;
}

export interface WeekGraphs {
    monday: WorkingHours;
    tuesday: WorkingHours;
    wednesday: WorkingHours;
    thursday: WorkingHours;
    friday: WorkingHours;
    saturday: WorkingHours;
    sunday: WorkingHours;
}

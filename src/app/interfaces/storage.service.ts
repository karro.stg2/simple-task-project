export interface StorageServiceInterface {

    set(key: string, value: string): void;
    get(key: string): string;
    remove(key: string): void;
}

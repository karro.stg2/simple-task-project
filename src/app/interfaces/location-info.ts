export interface LocationInfo {
    name: string;
    comercialName?: string;
    discount?: number;
    reservation?: boolean;
    categoryId?: string;
    averageFeeId?: string;
    descriptionRo?: string;
    descriptionRu?: string;
}

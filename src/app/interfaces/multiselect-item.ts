export interface MultiselectItem {
    id: string;
    itemName: string;
    image?: string;
}

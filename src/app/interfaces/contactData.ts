export interface ContactData {
    firstName: string;
    lastName: string;
    phoneNumber: string;
    country: string;
    city: string;
    street: string;
    zip: string;
}

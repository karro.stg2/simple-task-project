import { LocationInfo } from './location-info';
import { SelectItem } from './selectItem';
import { WorkingHours, WeekGraphs } from './workingHours';
import { ContactData, BusinessImages, MultiselectItem } from './index';

export interface APIServiceInterface {

    getLocationInfoDetails(): LocationInfo;
    setLocationInfoDetails(info: LocationInfo): void;

    getCategoriesList(): Array<SelectItem>;
    getFeesList(): Array<SelectItem>;

    getWorkingHours(): WeekGraphs;
    setWorkingHours(data: WeekGraphs): void;

    getContactData(): ContactData;
    setContactData(data: ContactData): void;

    getBusinessImages(): BusinessImages;
    setBusinessImages(data: BusinessImages): void;

    getFilterItems(): { kitchen: MultiselectItem[], local: MultiselectItem[], type: MultiselectItem[], menu: MultiselectItem[] };
    getFilteredSelectedItems(): { kitchen: MultiselectItem[], local: MultiselectItem[], type: MultiselectItem[], menu: MultiselectItem[] };
    setFilteredSelectedItems(
        data:
            {
                kitchen: MultiselectItem[], local: MultiselectItem[], type: MultiselectItem[], menu: MultiselectItem[]
            }
    ): void;

    getAdditionsList(): MultiselectItem[];
    getSelectedAdditionsList(): { available: MultiselectItem[], reservation: MultiselectItem[] };
    setSelectedAdditionsList(data: { available: MultiselectItem[], reservation: MultiselectItem[] }): void;
}

export interface BusinessImages {
    miniature: string;
    banner: string;
    gallery: string[];
}

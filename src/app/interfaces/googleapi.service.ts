import { Observable } from 'rxjs/Observable';

export interface GoogleApiServiceInterface {

    getDataCoord(data: string): Observable<any>;
    getDataDescription(data: string): Observable<any>;
}

import { Component, Input, OnInit, ViewChild, OnChanges, SimpleChanges, Output, EventEmitter } from '@angular/core';
import * as Cropper from 'cropperjs';
import { OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';

@Component({
    selector: '[st-image-cropper]',
    templateUrl: './image-cropper.component.html',
    styleUrls: ['./image-cropper.component.scss']
})
export class ImageCropperComponent implements OnChanges, OnDestroy {

    @Input('data') public data: any;
    @Input('viewOnly') public viewOnly = false;

    @ViewChild('croppedImage') private croppedImage: any;
    @ViewChild('previewCroppedImage') private previewCroppedImage: any;

    @ViewChild('onlyPreviewImage') private onlyPreviewImage: any;
    @ViewChild('onlyPreviewCropper') private onlyPreviewCropper: any;

    @Output('cropboxInstance') public cropboxInstance = new EventEmitter<any>();

    private cropperBox: any;

    initView() {

        if (this.viewOnly) {
            this.setPreviewImageSrc(this.onlyPreviewImage.nativeElement);
            this.setPreviewImageSrc(this.onlyPreviewCropper.nativeElement);
            this.setupImageCrop(this.onlyPreviewCropper.nativeElement);
        } else {
            this.setPreviewImageSrc(this.croppedImage.nativeElement);
            this.setupImageCrop(
                this.croppedImage.nativeElement,
                this.previewCroppedImage.nativeElement
            );
        }
    }

    ngOnChanges(changes: SimpleChanges) {

        if (changes['data']) {

            this.initView();
        }
    }

    ngOnDestroy() {

        this.destroyCropbox();
        this.cropboxInstance.emit(this.cropperBox);
    }

    setPreviewImageSrc(element: any) {

        element.src = null; // force reload
        element.src = this.data;
    }

    destroyCropbox() {

        if (this.cropperBox) {
            this.cropperBox.destroy();
            this.cropperBox = null;
        }
    }

    setupImageCrop(element: any, preview?: any) {

        this.destroyCropbox();
        this.cropperBox = new Cropper(element, {
            preview: preview ? preview : undefined,
            aspectRatio: 1 / 1,
            modal: false,
            viewMode: 2
        });

        this.cropboxInstance.emit(this.cropperBox);
    }

}

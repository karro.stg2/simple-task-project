import { NgModule } from '@angular/core';

import { ImageCropperComponent } from './image-cropper.component';

@NgModule({
    imports: [],
    declarations: [
        ImageCropperComponent
    ],
    exports: [
        ImageCropperComponent
    ]
})
export class ImageCropperModule { }

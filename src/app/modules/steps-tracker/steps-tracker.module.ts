import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { StepsTrackerComponent } from './steps-tracker.component';

@NgModule({
    declarations: [
        StepsTrackerComponent,
    ],
    imports: [CommonModule],
    exports: [StepsTrackerComponent]
})
export class StepsTrackerModule {

}

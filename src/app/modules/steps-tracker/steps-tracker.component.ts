import { Component, OnInit, Input } from '@angular/core';

@Component({
    selector: '[st-steps-tracker]',
    templateUrl: './steps-tracker.component.html',
    styleUrls: ['./steps-tracker.component.scss']
})
export class StepsTrackerComponent implements OnInit {

    @Input('count') public count = 0;
    @Input('active') public active = 0;

    public counterArray = [];

    ngOnInit() {

        this.setupItems();
    }

    setupItems() {

        this.counterArray = new Array(this.count * 2 - 1)
            .fill(0.5)
            .map((v, i) => i * v + 1);

    }
}

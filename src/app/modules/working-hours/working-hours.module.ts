import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { WorkingHoursComponent } from './working-hours.component';
import { DirectivesModule } from '../../directives/directives.module';

@NgModule({
    declarations: [
        WorkingHoursComponent,
    ],
    imports: [
        CommonModule,
        FormsModule,
        DirectivesModule,
    ],
    exports: [
        WorkingHoursComponent
    ]
})
export class WorkingHoursModule {

}

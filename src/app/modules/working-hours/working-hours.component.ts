import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';

import { WorkingHours, WorkingHoursState } from '../../interfaces/workingHours';


@Component({
    selector: '[st-working-hours]',
    templateUrl: './working-hours.component.html',
    styleUrls: ['./working-hours.component.scss']
})
export class WorkingHoursComponent implements OnInit {

    @Input('data') public data: WorkingHours;
    @Input('invalid') public invalid: boolean;
    @Output('dataChanged') public dataChanged = new EventEmitter<WorkingHours>();

    public workingStates = WorkingHoursState;

    constructor() { }

    ngOnInit() {

        if (this.data.state !== WorkingHoursState.Custom) {
            this.data.timeFrom = '';
            this.data.timeTo = '';
            this.onChange();
        }
    }

    onChange() {

        if (this.validate()) {
            this.invalid = false;
            this.dataChanged.emit(this.data);
        } else {
            this.invalid = true;
        }
    }

    isClosed() {

        this.data.state = WorkingHoursState.Closed;
        this.data.timeFrom = '';
        this.data.timeTo = '';
        this.onChange();
    }

    isOpened() {

        this.data.state = WorkingHoursState.Custom;
    }

    isFullOpened() {

        this.data.state = WorkingHoursState.Full;
        this.data.timeFrom = '';
        this.data.timeTo = '';
        this.onChange();
    }

    validate() {

        if (this.data.state === WorkingHoursState.Custom) {
            return this.validateDate(this.data.timeFrom)
                && this.validateDate(this.data.timeTo);
        }
        return true;
    }

    validateDate(value: string) {

        try {

            const time = value.split(':');
            const hh = parseInt(time[0], 10);
            const mm = parseInt(time[1], 10);

            return time.length === 2
                && time[0].length === 2
                && time[1].length === 2
                && hh < 24
                && mm < 60;

        } catch (e) {
            return false;
        }
    }
}

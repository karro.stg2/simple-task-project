import { ComponentFactoryResolver, Injectable } from '@angular/core';

import { UiLoaderServiceInterface } from './ui-loader.interface';

@Injectable()
export class UiLoaderService implements UiLoaderServiceInterface {

    constructor(
        private componentFactoryResolver: ComponentFactoryResolver
    ) { }

    createComponent(component: any) {

        return this.componentFactoryResolver.resolveComponentFactory(component);
    }

}

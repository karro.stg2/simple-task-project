import { Component } from '@angular/core';

@Component({
    moduleId: module.id,
    selector: '[st-ui-loader]',
    templateUrl: 'ui-loader.component.html',
    styleUrls: ['ui-loader.component.css'],
})
export class UiLoaderComponent {

    constructor() { }

}

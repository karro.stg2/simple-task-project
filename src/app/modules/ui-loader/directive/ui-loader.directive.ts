import { ComponentFactory, Directive, ElementRef, Inject, Input, OnChanges, Renderer2, SimpleChanges } from '@angular/core';
import { ViewContainerRef } from '@angular/core';

import { UiLoaderComponent } from '../component/ui-loader.component';
import { UiLoaderServiceInterface } from '../service/ui-loader.interface';
import { UiLoaderService } from '../service/ui-loader.service';


@Directive({
    selector: '[stUiLoader]',
})
export class UiLoaderDirective implements OnChanges {

    @Input('loading') public loading: boolean;

    private componentFactory: ComponentFactory<UiLoaderComponent>;

    readonly loadingClassname = 'ui-wrapper--blured';


    constructor(

        @Inject(UiLoaderService) private loaderService: UiLoaderServiceInterface,
        private viewContainerRef: ViewContainerRef,
        private renderer: Renderer2,
        private element: ElementRef,
    ) {

        this.componentFactory = this.loaderService.createComponent(UiLoaderComponent);
    }


    ngOnChanges(change: SimpleChanges) {

        this.checkLoading(change['loading'].currentValue);
    }

    checkLoading(state: boolean) {

        if (state) {
            this.showLoader();
        } else {
            this.deleteLoader();
        }
    }

    showLoader() {

        this.viewContainerRef.createComponent(this.componentFactory);
        this.renderer.addClass(this.element.nativeElement, this.loadingClassname);
    }

    deleteLoader() {

        this.viewContainerRef.clear();
        this.renderer.removeClass(this.element.nativeElement, this.loadingClassname);
    }

}


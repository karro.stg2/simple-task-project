import { UiLoaderComponent } from './component/ui-loader.component';
import { NgModule } from '@angular/core';

import { UiLoaderDirective } from './directive/ui-loader.directive';
import { UiLoaderService } from './service/ui-loader.service';

@NgModule({
    declarations: [UiLoaderDirective, UiLoaderComponent],
    providers: [UiLoaderService],
    exports: [UiLoaderDirective, UiLoaderComponent],
    entryComponents: [UiLoaderComponent]
})
export class UiLoaderModule { }

import { NgModule } from '@angular/core';

import { HourInputDirective } from './hourInput.directive';

@NgModule({
    declarations: [HourInputDirective],
    exports: [HourInputDirective]
})
export class DirectivesModule { }

import { Directive, ElementRef, HostListener } from '@angular/core';

@Directive({
    selector: '[stHourInput]'
})
export class HourInputDirective {

    private regexStr = /(([0-1]\d?)|([2][0-3]?))?(:[0-5]?\d?)?/;

    constructor(private el: ElementRef) { }

    @HostListener('input', ['$event'])
    onKeyDown(event) {

        const current: string = this.el.nativeElement.value;

        const result = this.regexStr.exec(current);

        if (result && result.length) {
            this.el.nativeElement.value = result[0];
        } else {
            this.el.nativeElement.value = null;
        }
        return;
    }
}

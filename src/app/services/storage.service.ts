import { Injectable } from '@angular/core';

import { StorageServiceInterface } from '../interfaces/index';

@Injectable()
export class StorageService implements StorageServiceInterface {

    private _storage = localStorage;

    public set(key: string, value: string): void {

        this._storage.setItem(key, value);
    }

    public get(key: string): string {

        return JSON.parse(this._storage.getItem(key));
    }

    public remove(key: string) {

        this._storage.removeItem(key);
    }
}

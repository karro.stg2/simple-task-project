import { MockedClassInterface } from '../interfaces/index';
import { LocationInfo } from '../interfaces/location-info';
import { StorageService } from './storage.service';
import { WeekGraphs, WorkingHoursState } from '../interfaces/workingHours';

export default function MockData(scope: string) {

    return (target, key, descriptor) => {

        if (descriptor === undefined) {
            descriptor = Object.getOwnPropertyDescriptor(target, key);
        }

        descriptor.value = getMockDataValue(scope);

        return descriptor;
    };
}

function getMockDataValue(scope: string) {

    const availableIntances = {
        locationDetails: LocationDetails,
        categoriesList: CategoriesList,
        feesList: FeesList,
        workingHours: WorkingHours,
        contactDataDetails: ContactDataDetails,
        businessImages: BusinessImages,
        filteredOptions: FilteredOptions,
        filteredSelectedOptions: FilteredSelectedOptions,
        additionsList: AdditionsList,
        selectedAdditionsList: SelectedAdditionsList,
    };

    return () => (new availableIntances[scope]()).getProperties();

}

const storage = new StorageService();

class MockedClass implements MockedClassInterface {

    constructor(private _storageKey: string) {

    }

    getProperties() {

        let result = this;

        const data = storage.get(this._storageKey);

        if (data) {

            result = Object.assign({}, result, data);
        }
        delete result._storageKey;

        return result;
    }
}

class LocationDetails extends MockedClass implements LocationInfo {

    name = 'Star Holding';
    comercialName: string;
    discount: number;
    reservation: boolean;
    categoryId: string;
    averageFeeId: string;
    descriptionRo: string;
    descriptionRu: string;

    constructor() {
        super('locationDetails');
    }

}

class CategoriesList implements MockedClassInterface {

    constructor() { }

    getProperties() {

        return [
            { label: 'Educatie', value: 'ed' },
            { label: 'Cultura', value: 'cu' },
            { label: 'Divertisment', value: 'ds' },
            { label: 'Business', value: 'business' },
            { label: 'Alta', value: 'alta' },
        ];
    }
}

class FeesList implements MockedClassInterface {

    constructor() { }

    getProperties() {

        return [
            { label: '100 - 200', value: 1 },
            { label: '201 - 500', value: 2 },
            { label: '501 - 1000', value: 3 },
            { label: '1001 - 5000', value: 4 },
            { label: '5000 +', value: 5 },
        ];
    }
}

class WorkingHours extends MockedClass implements MockedClassInterface {

    monday = {
        state: WorkingHoursState.Custom,
        timeFrom: '00:00',
        timeTo: '00:00',
    };
    tuesday = {
        state: WorkingHoursState.Custom,
        timeFrom: '00:00',
        timeTo: '00:00',
    };
    wednesday = {
        state: WorkingHoursState.Custom,
        timeFrom: '00:00',
        timeTo: '00:00',
    };
    thursday = {
        state: 'Closed',
        timeFrom: '00:00',
        timeTo: '00:00',
    };
    friday = {
        state: WorkingHoursState.Custom,
        timeFrom: '00:00',
        timeTo: '00:00',
    };
    saturday = {
        state: WorkingHoursState.Custom,
        timeFrom: '00:00',
        timeTo: '00:00',
    };
    sunday = {
        state: WorkingHoursState.Custom,
        timeFrom: '00:00',
        timeTo: '00:00',
    };


    constructor() {
        super('workingHours');
    }

}

class ContactDataDetails extends MockedClass implements MockedClassInterface {

    constructor() {
        super('contactDataDetails');
    }
}
class BusinessImages extends MockedClass implements MockedClassInterface {

    banner = null;
    gallery = [];
    miniature = null;

    constructor() {
        super('businessImages');
    }
}

class FilteredOptions implements MockedClassInterface {


    constructor() {
    }

    getProperties() {
        return {
            kitchen:
                [
                    { 'id': 1, 'itemName': 'India' },
                    { 'id': 2, 'itemName': 'Singapore' },
                    { 'id': 3, 'itemName': 'Australia' },
                    { 'id': 4, 'itemName': 'Canada' },
                    { 'id': 5, 'itemName': 'South Korea' },
                    { 'id': 6, 'itemName': 'Germany' },
                    { 'id': 7, 'itemName': 'France' },
                    { 'id': 8, 'itemName': 'Russia' },
                    { 'id': 9, 'itemName': 'Italy' },
                    { 'id': 10, 'itemName': 'Sweden' }
                ],
            local: [
                { 'id': 1, 'itemName': 'Terasa' },
                { 'id': 2, 'itemName': 'Other Location' }
            ],
            type: [
                { 'id': 1, 'itemName': '1 pers' },
                { 'id': 2, 'itemName': '2 pers' }
            ],
            menu: [
                { 'id': 1, 'itemName': 'Pentru bere' },
                { 'id': 2, 'itemName': 'Business lunch' },
                { 'id': 3, 'itemName': 'pentru bere' },
                { 'id': 4, 'itemName': 'business lunch' }
            ]
        };
    }
}

class FilteredSelectedOptions extends MockedClass implements MockedClassInterface {

    kitchen = [
        { 'id': 5, 'itemName': 'South Korea' },
        { 'id': 8, 'itemName': 'Russia' },
    ];
    local = [
        { 'id': 1, 'itemName': 'Terasa' },
    ];
    type = [];
    menu = [
        { 'id': 1, 'itemName': 'Pentru bere' },
    ];

    constructor() {
        super('filteredSelectedOptions');
    }

}


class AdditionsList implements MockedClassInterface {


    constructor() {
    }

    getProperties() {

        return [
            {
                'id': 1,
                'itemName': 'Business Lunches',
                'image': 'https://placehold.it/100x100'
            },
            {
                'id': 2,
                'itemName': 'Terasa',
                'image': 'https://placehold.it/100x100'
            },
            {
                'id': 3,
                'itemName': 'Livrare gratuita',
                'image': 'https://placehold.it/100x100'
            },
            {
                'id': 4,
                'itemName': 'Parcare Subterana',
                'image': 'https://placehold.it/100x100'
            }
        ];
    }
}


class SelectedAdditionsList extends MockedClass implements MockedClassInterface {
    available = [];
    reservation = [];

    constructor() {
        super('selectedAdditionsList');
    }
}


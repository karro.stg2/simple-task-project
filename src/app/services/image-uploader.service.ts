import { Injectable } from '@angular/core';

import { IMGUR_API_KEY } from '../constants/index';
import { ImageUploaderInterface } from '../interfaces/image-uploader.service';

@Injectable()
export class ImageUploaderService implements ImageUploaderInterface {

    upload(data: Blob) {
        return new Promise((resolve, reject) => {

            const url = 'https://api.imgur.com/3/image';
            const formData: any = new FormData();
            const xhr = new XMLHttpRequest();

            formData.append('image', data, 'image');

            xhr.onreadystatechange = () => {
                if (xhr.readyState == 4) {
                    if (xhr.status == 200) {
                        resolve(JSON.parse(xhr.response));
                    } else {
                        reject({
                            status: xhr.status,
                            message: JSON.parse(xhr.response)
                        });
                    }
                }
            };
            xhr.open('POST', url, true);
            xhr.setRequestHeader(
                'Authorization',
                `Client-ID ${IMGUR_API_KEY}`
            );

            xhr.send(formData);
        });
    }
}

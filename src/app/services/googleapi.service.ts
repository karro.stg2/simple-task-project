import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { GOOGLE_MAPS_API_KEY } from '../constants/index';
import { GoogleApiServiceInterface } from '../interfaces/index';

@Injectable()
export class GoogleApiService implements GoogleApiServiceInterface {

    constructor(private http: HttpClient) { }

    getDataCoord(data: string) {

        const url = `https://maps.googleapis.com/maps/api/geocode/json` +
            `?address=${data}`
            + `&key=${GOOGLE_MAPS_API_KEY}`;

        return this.http
            .get(url, { observe: 'response' });
    }

    getDataDescription(data: string) {

        const url = `https://maps.googleapis.com/maps/api/geocode/json`
            + `?latlng=${data}`
            + `&key=${GOOGLE_MAPS_API_KEY}`;

        return this.http
            .get(url, { observe: 'response' });
    }
}

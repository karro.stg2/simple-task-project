import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';

import { APIServiceInterface, BusinessImages, ContactData, StorageServiceInterface } from '../interfaces/index';
import { LocationInfo } from '../interfaces/location-info';
import { WeekGraphs } from '../interfaces/workingHours';
import MockData from './mockdata.service';
import { StorageService } from './storage.service';


@Injectable()
export class APIService implements APIServiceInterface {

    constructor(
        private http: HttpClient,
        @Inject(StorageService) private storage: StorageServiceInterface
    ) {

    }

    @MockData('locationDetails')
    getLocationInfoDetails() {

        return {} as any;
    }

    setLocationInfoDetails(data: LocationInfo) {

        return new Promise((resolve, reject) => {
            this.storage.set('locationDetails', JSON.stringify(data));
            resolve();
        });
    }

    setWorkingHours(data: WeekGraphs) {

        return new Promise((resolve, reject) => {
            this.storage.set('workingHours', JSON.stringify(data));
            resolve();
        });
    }

    setContactData(data: ContactData) {

        return new Promise((resolve, reject) => {
            this.storage.set('contactDataDetails', JSON.stringify(data));
            resolve();
        });
    }

    setBusinessImages(data: BusinessImages) {

        return new Promise((resolve, reject) => {
            this.storage.set('businessImages', JSON.stringify(data));
            resolve();
        });
    }


    setFilteredSelectedItems(data: any) {
        return new Promise((resolve, reject) => {
            this.storage.set('filteredSelectedOptions', JSON.stringify(data));
            resolve();
        });
    }

    setSelectedAdditionsList(data: any) {
        return new Promise((resolve, reject) => {
            this.storage.set('selectedAdditionsList', JSON.stringify(data));
            resolve();
        });
    }

    @MockData('categoriesList')
    getCategoriesList() {

        return {} as any;
    }

    @MockData('feesList')
    getFeesList() {

        return {} as any;
    }

    @MockData('workingHours')
    getWorkingHours(): WeekGraphs {

        return {} as any;
    }

    @MockData('contactDataDetails')
    getContactData(): ContactData {

        return {} as any;
    }

    @MockData('businessImages')
    getBusinessImages(): BusinessImages {

        return {} as any;
    }

    @MockData('filteredOptions')
    getFilterItems() {

        return {} as any;
    }

    @MockData('filteredSelectedOptions')
    getFilteredSelectedItems() {

        return {} as any;
    }

    @MockData('additionsList')
    getAdditionsList() {

        return {} as any;
    }

    @MockData('selectedAdditionsList')
    getSelectedAdditionsList() {

        return {} as any;
    }

}

import { Component } from '@angular/core';

@Component({
  selector: 'st-app',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
}

import { Component, Inject, OnInit } from '@angular/core';

import { APIServiceInterface } from '../../../interfaces/index';
import { APIService } from '../../../services/api.service';
import RegisterStepSkeleton from '../register-step.skeleton';

@Component({
    selector: '[st-register-step-6]',
    templateUrl: './register-step-6.component.html',
    styleUrls: ['./register-step-6.component.scss']
})
export class RegisterStep6Component extends RegisterStepSkeleton implements OnInit {

    constructor(
        @Inject(APIService) private api: APIServiceInterface
    ) {
        super();
    }

    dropdownList = [];

    selected = {
        available: [],
        reservation: []
    };

    dropdownSettings = {};
    ngOnInit(

    ) {
        this.dropdownList = this.api.getAdditionsList();
        this.selected = this.api.getSelectedAdditionsList();

        this.dropdownSettings = {
            singleSelection: false,
            enableSearchFilter: false,
            enableCheckAll: false,
            text: '',
            classes: 'custom-dropdown',
        };
    }

    save() {

        this.api.setSelectedAdditionsList(this.selected);
        this.nextStep.emit(7);
    }

    cancel() {

        this.selected = this.api.getSelectedAdditionsList();
    }

    back() {

        this.nextStep.emit(5);
    }
}

import { Component, Inject, OnInit } from '@angular/core';

import { ImageUploaderInterface } from '../../../interfaces/image-uploader.service';
import { ImageUploaderService } from '../../../services/image-uploader.service';
import RegisterStepSkeleton from '../register-step.skeleton';
import { APIService } from '../../../services/api.service';
import { APIServiceInterface } from '../../../interfaces/index';

@Component({
    selector: '[st-register-step-4]',
    templateUrl: './register-step-4.component.html',
    styleUrls: ['./register-step-4.component.scss']
})
export class RegisterStep4Component extends RegisterStepSkeleton implements OnInit {

    public images = {
        miniature: null,
        banner: null,
        gallery: []
    };

    private cropBoxes = {
        miniature: null,
        banner: null,
        gallery: []
    };

    public loading = false;

    constructor(
        @Inject(ImageUploaderService) private imageUploader: ImageUploaderInterface,
        @Inject(APIService) private api: APIServiceInterface
    ) {
        super();
    }

    ngOnInit() {

        this.images = this.api.getBusinessImages();
    }


    public async onImageChange(event: any, imageId: string) {

        const file = event.target.files[0];
        if (imageId === 'gallery') {
            this.images.gallery.push(await this.getPreviewSrc(file));
        } else {
            this.images[imageId] = await this.getPreviewSrc(file);
        }
    }


    private async setupImagePreview(file: any, previewBox: any) {

        previewBox.src = await this.getPreviewSrc(file);
        return;
    }

    private getPreviewSrc(file) {

        return new Promise((resolve, reject) => {

            const reader = new FileReader();

            reader.onload = (e: any) => {
                resolve(e.target.result);
            };

            reader.readAsDataURL(file);
        });
    }

    public removeFromGallery() {

        this.images.gallery.pop();
    }

    saveCropbox(cropbox: any, imageId: string) {

        if (imageId === 'gallery') {
            if (!cropbox) {
                this.cropBoxes.gallery.pop();
            } else {
                this.cropBoxes.gallery.push(cropbox);
            }
        } else {
            this.cropBoxes[imageId] = cropbox;
        }
    }

    async saveData() {

        this.loading = true;
        const galleryBlobs = [];
        for (const cropBox of this.cropBoxes.gallery) {
            galleryBlobs.push(this.getCroppedBlob(cropBox));
        }

        const blobs = {
            miniature: await this.getCroppedBlob(this.cropBoxes.miniature),
            banner: await this.getCroppedBlob(this.cropBoxes.banner),
            gallery: await Promise.all(galleryBlobs)
        };
        const urls = {
            miniature: blobs.miniature ? (await this.uploadOne(blobs.miniature)).data.link : undefined,
            banner: blobs.banner ? (await this.uploadOne(blobs.banner)).data.link : undefined,
            gallery: blobs.gallery ? await this.bulkUpload(blobs.gallery) : []
        };


        this.api.setBusinessImages(urls);
        this.loading = false;
        this.nextStep.emit(5);
    }

    getCroppedBlob(cropper): Promise<any> {

        return new Promise((resolve, reject) => {

            if (!cropper) {
                resolve(undefined);
            }
            cropper.getCroppedCanvas({
                width: 160,
                height: 90,
                minWidth: 256,
                minHeight: 256,
                maxWidth: 1080,
                maxHeight: 807,
                fillColor: '#fff',
                imageSmoothingEnabled: false,
                imageSmoothingQuality: 'high',
            });

            cropper.getCroppedCanvas().toBlob((blob) => {
                resolve(blob);
            });
        });
    }

    async bulkUpload(files: any[]) {

        if (!files || !files.length) {
            return undefined;
        }

        let queue = [];
        for (const file of files) {
            queue.push(this.uploadOne(file));
        }
        queue = await Promise.all(queue);
        return queue
            .filter(q => q.success)
            .map(f => f.data.link);

    }

    async uploadOne(file: any) {

        return await this.imageUploader.upload(file);
    }

    back() {

        this.nextStep.emit(3);
    }

    cancel() {

        this.images = this.api.getBusinessImages();
    }
}

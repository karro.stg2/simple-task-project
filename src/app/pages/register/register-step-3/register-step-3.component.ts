import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

import { TEXT_INPUT_VALIDATORS } from '../../../constants/index';
import { APIServiceInterface, GoogleApiServiceInterface } from '../../../interfaces/index';
import { APIService } from '../../../services/api.service';
import { GoogleApiService } from '../../../services/googleapi.service';
import RegisterStepSkeleton from '../register-step.skeleton';

@Component({
    selector: '[st-register-step-3]',
    templateUrl: './register-step-3.component.html',
    styleUrls: ['./register-step-3.component.scss']
})
export class RegisterStep3Component extends RegisterStepSkeleton implements OnInit {

    lat = 47.022282;
    lng = 28.840711;

    zoom = 14;

    public contactForm: FormGroup;

    constructor(
        @Inject(APIService) private api: APIServiceInterface,
        @Inject(GoogleApiService) private gApi: GoogleApiServiceInterface
    ) {
        super();
    }

    ngOnInit() {

        this.setupForm();
    }

    setupForm() {

        const data = this.api.getContactData();

        this.contactForm = new FormGroup({
            firstName: new FormControl(data.firstName || '', TEXT_INPUT_VALIDATORS),
            lastName: new FormControl(data.lastName || '', TEXT_INPUT_VALIDATORS),
            phoneNumber: new FormControl(data.phoneNumber || '', TEXT_INPUT_VALIDATORS),
            country: new FormControl(data.country || '', TEXT_INPUT_VALIDATORS),
            city: new FormControl(data.city || '', TEXT_INPUT_VALIDATORS),
            street: new FormControl(data.street || '', TEXT_INPUT_VALIDATORS),
            zip: new FormControl(data.zip || '', TEXT_INPUT_VALIDATORS),
        });
    }

    submitForm() {

        if (!this.checkFormValidity()) {
            this.api.setContactData(this.contactForm.value);
            this.nextStep.emit(4);
        }

    }

    mapClicked(e) {

        const { coords } = e;
        this.gApi.getDataDescription(`${coords.lat},${coords.lng}`)
            .subscribe((rs) => {
                this.updateDescription(rs.body);
                this.lat = coords.lat;
                this.lng = coords.lng;
            });
    }

    geoInputChanged() {

        const data = [];
        const formData = this.contactForm.value;

        data.push([formData.country, formData.city, formData.street, formData.zip]);

        const searchString = data.join(',').split(' ').join('+');
        this.gApi.getDataCoord(searchString)
            .subscribe((rs) => {
                this.updateGeoPosition(rs.body);
            });
    }

    updateDescription(body: any) {

        if (body.results && body.results.length) {
            let addr = body.results[0].formatted_address;
            addr = addr.split(',');
            this.contactForm.controls['country'].setValue(addr[addr.length - 1]);
            if (addr.length > 1) {
                this.contactForm.controls['city'].setValue(addr[addr.length - 2]);
            }
            if (addr.length > 2) {
                this.contactForm.controls['street'].setValue(addr[addr.length - 3]);
            }
        }
    }

    updateGeoPosition(body: any) {

        if (body.results && body.results.length) {
            const coord = body.results[0].geometry.location;
            this.lat = coord.lat;
            this.lng = coord.lng;
        }
    }

    checkFormValidity() {

        let hasErrors = false;
        // tslint:disable-next-line:forin
        for (const i in this.contactForm.controls) {
            const control = this.contactForm.controls[i];
            if (control.errors) {
                control.markAsTouched();
                hasErrors = true;
            }
        }
        return hasErrors;
    }

    back() {

        this.nextStep.emit(2);
    }
}

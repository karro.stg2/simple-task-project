import { Component, Inject, OnInit } from '@angular/core';

import { APIServiceInterface } from '../../../interfaces/api.service';
import { APIService } from '../../../services/api.service';
import RegisterStepSkeleton from '../register-step.skeleton';

@Component({
    selector: '[st-register-step-2]',
    templateUrl: './register-step-2.component.html',
    styleUrls: ['./register-step-2.component.scss']
})
export class RegisterStep2Component extends RegisterStepSkeleton implements OnInit {

    public workingHours;
    private _workingHoursCopy;

    constructor(
        @Inject(APIService) private api: APIServiceInterface
    ) {
        super();
    }

    ngOnInit() {

        this.setWorkingHours();
    }

    setWorkingHours() {

        this.workingHours = this.api.getWorkingHours();
        this._workingHoursCopy = JSON.parse(JSON.stringify(this.workingHours));
    }

    submit() {

        this.api.setWorkingHours(this.workingHours);
        this.nextStep.emit(3);
    }

    cancelForm() {

        this.api.setWorkingHours(this._workingHoursCopy);
        this.setWorkingHours();
    }

    back() {

        this.nextStep.emit(1);
    }
}

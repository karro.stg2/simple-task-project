import { Component } from '@angular/core';

@Component({
    selector: 'st-register',
    templateUrl: './register.component.html',
    styleUrls: ['./register.component.scss']
})
export class RegisterComponent {

    public activeStep = 1;

    constructor() { }

    changeStep(value: number) {

        this.activeStep = value;
    }
}

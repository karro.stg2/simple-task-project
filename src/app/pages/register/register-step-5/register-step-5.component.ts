import { Component, Inject, OnInit } from '@angular/core';

import { APIServiceInterface } from '../../../interfaces/index';
import { APIService } from '../../../services/api.service';
import RegisterStepSkeleton from '../register-step.skeleton';

@Component({
    selector: '[st-register-step-5]',
    templateUrl: './register-step-5.component.html',
    styleUrls: ['./register-step-5.component.scss']
})
export class RegisterStep5Component extends RegisterStepSkeleton implements OnInit {

    constructor(
        @Inject(APIService) private api: APIServiceInterface
    ) {
        super();
    }

    selected = {
        kitchen: [],
        local: [],
        type: [],
        menu: []
    };
    dropdownSettings = {};

    dropdownOptions;
    ngOnInit() {

        this.dropdownOptions = this.api.getFilterItems();
        this.setSelectedItems();
        this.dropdownSettings = {
            singleSelection: false,
            enableSearchFilter: false,
            enableCheckAll: false,
            text: '',
            classes: 'custom-dropdown',
        };
    }

    setSelectedItems() {

        this.selected = this.api.getFilteredSelectedItems();
    }

    save() {

        this.api.setFilteredSelectedItems(this.selected);
        this.nextStep.emit(6);
    }

    cancel() {

        this.setSelectedItems();
    }

    back() {

        this.nextStep.emit(4);
    }
}

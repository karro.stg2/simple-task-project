import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

import { DISCOUNT_VALIDATORS, TEXT_AREA_VALIDATORS, TEXT_INPUT_VALIDATORS } from '../../../constants/index';
import { APIServiceInterface } from '../../../interfaces/api.service';
import { APIService } from '../../../services/api.service';
import RegisterStepSkeleton from '../register-step.skeleton';

@Component({
    selector: '[st-register-step-1]',
    templateUrl: './register-step-1.component.html',
    styleUrls: ['./register-step-1.component.scss']
})
export class RegisterStep1Component extends RegisterStepSkeleton implements OnInit {

    public registrationForm: FormGroup;

    public categories: any;
    public fees: any;


    constructor(
        @Inject(APIService) private api: APIServiceInterface
    ) {
        super();
    }

    ngOnInit() {

        this.setFormControls();
        this.setCategories();
        this.setFees();
    }

    setFees() {

        this.fees = this.api.getFeesList();
    }

    setCategories() {

        this.categories = this.api.getCategoriesList();
    }

    setFormControls() {

        const formData = this.api.getLocationInfoDetails();

        this.registrationForm = new FormGroup({
            name: new FormControl({ value: formData.name, disabled: true }, TEXT_INPUT_VALIDATORS),
            comercialName: new FormControl(formData.comercialName || '', TEXT_INPUT_VALIDATORS),
            discount: new FormControl(formData.discount || '', DISCOUNT_VALIDATORS),
            reservation: new FormControl(formData.reservation || false, TEXT_INPUT_VALIDATORS),
            categoryId: new FormControl(formData.categoryId || '', TEXT_INPUT_VALIDATORS),
            averageFeeId: new FormControl(formData.averageFeeId || '', TEXT_INPUT_VALIDATORS),
            descriptionRo: new FormControl(formData.descriptionRo || '', TEXT_AREA_VALIDATORS),
            descriptionRu: new FormControl(formData.descriptionRu || '', TEXT_AREA_VALIDATORS),
        });

    }

    submitForm() {

        if (this.checkFormValidity()) {
            return;
        }

        this.api.setLocationInfoDetails(this.registrationForm.value);
        this.nextStep.emit(2);
    }

    checkFormValidity() {

        let hasErrors = false;
        // tslint:disable-next-line:forin
        for (const i in this.registrationForm.controls) {
            const control = this.registrationForm.controls[i];
            if (control.errors) {
                control.markAsTouched();
                hasErrors = true;
            }
        }
        return hasErrors;
    }

    cancelForm() {

        this.api.setLocationInfoDetails({ name: this.registrationForm.value['name'] });
        this.setFormControls();
    }
}

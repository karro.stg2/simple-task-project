import { AgmCoreModule } from '@agm/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';

import { GOOGLE_MAPS_API_KEY } from '../../constants/index';
import { ImageCropperModule } from '../../modules/image-cropper/image-cropper.module';
import { StepsTrackerModule } from '../../modules/steps-tracker/steps-tracker.module';
import { UiLoaderModule } from '../../modules/ui-loader/ui-loader.module';
import { WorkingHoursModule } from '../../modules/working-hours/working-hours.module';
import { APIService } from '../../services/api.service';
import { GoogleApiService } from '../../services/googleapi.service';
import { ImageUploaderService } from '../../services/image-uploader.service';
import { StorageService } from '../../services/storage.service';
import { SharedModule } from '../../shared/shared.module';
import { RegisterStep1Component } from './register-step-1/register-step-1.component';
import { RegisterStep2Component } from './register-step-2/register-step-2.component';
import { RegisterStep3Component } from './register-step-3/register-step-3.component';
import { RegisterStep4Component } from './register-step-4/register-step-4.component';
import { RegisterStep5Component } from './register-step-5/register-step-5.component';
import { RegisterStep6Component } from './register-step-6/register-step-6.component';
import { RegisterStep7Component } from './register-step-7/register-step-7.component';
import { RegisterComponent } from './register.component';

@NgModule({
    imports: [
        CommonModule,
        BrowserModule,
        SharedModule,
        FormsModule,
        StepsTrackerModule,
        HttpClientModule,
        AgmCoreModule.forRoot({
            apiKey: GOOGLE_MAPS_API_KEY
        }),
        AngularMultiSelectModule,
        ReactiveFormsModule,
        WorkingHoursModule,
        ImageCropperModule,
        UiLoaderModule,
    ],
    declarations: [
        RegisterComponent,
        RegisterStep1Component,
        RegisterStep2Component,
        RegisterStep3Component,
        RegisterStep4Component,
        RegisterStep5Component,
        RegisterStep6Component,
        RegisterStep7Component,
    ],
    providers: [
        StorageService,
        APIService,
        GoogleApiService,
        ImageUploaderService,
    ],
    exports: [
        RegisterComponent,
    ]
})
export class RegisterModule {

}

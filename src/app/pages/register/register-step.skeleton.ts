import { EventEmitter, Input, Output } from '@angular/core';

export default class RegisterStepSkeleton {

    @Output('nextStep') public nextStep = new EventEmitter<any>();

}

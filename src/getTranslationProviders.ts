import { TRANSLATIONS, TRANSLATIONS_FORMAT } from '@angular/core';

import { environment } from './environments/environment';

declare const require;

// translations used only for local environment


export function getTranslationProviders(): Promise<any[]> {

    const noProviders = [];

    // provide translation declarations only for JIT compile
    if (environment.ENV !== 'LOCAL') {
        return Promise.resolve(noProviders);
    }

    return Promise.resolve([
        { provide: TRANSLATIONS, useValue: getTranslations() },
        { provide: TRANSLATIONS_FORMAT, useValue: 'xlf' }
    ]);
}


function getTranslations() {
    return require(`raw-loader!./i18n/messages.en.xlf`);
}

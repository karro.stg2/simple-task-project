import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';
import { getTranslationProviders } from './getTranslationProviders';

if (environment.ENV === 'PROD') {
  enableProdMode();
}

getTranslationProviders()
  .then((providers) => {

    platformBrowserDynamic().bootstrapModule(AppModule, {
      providers
    });
  });

